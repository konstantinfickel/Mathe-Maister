# General TODOs
### CSV to PDF Skript
 + show date of last edit
 + show name of document, i.e. the lecture name

### CSV to APKG Skript
 + change options, including leech limit and limit of daily repetitions and new cards
 + create a version that bundles all decks in one file for quick importing (useful at all?)
 + "Created" in Anki currently showing Unix 0 (i.e. 1970-01-01) (probably never gonna be fixed)

### Andere
 - Gästebuch
 - Datum im PDF
 - Stoffsammlungen mit Priority für jede Zeile (1, 2, 3?) für kleinere Decks