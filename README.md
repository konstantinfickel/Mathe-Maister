# Mathe-Maister
Ansammlung einiger wichtiger themenübergreifender mathematischer Definitionen und Sätze, später dann verpackt in Anki in schönem Latex, um die Sprache der Mathematik fließender zu verstehen.


## Aktuelle Version:
**1.0.12**     
Vergangene und geplante Änderungen einsehbar in den [Patch Notes](Anleitungen-Links-etc/Patch-Notes.md)     
**Achtung!** Vor Version 1.0.0 verfügbare Decks sind nicht mehr kompatibel mit der automatischen Aktualisierung durch den Anki-apkg-Import (Standard Deckimport durch Öffnen der Datei etc) der neuen Decks seit Version 1.0.0.     
Falls du ein Deck von vor Version 1.0.0 aktualisieren möchtest (also nicht über einen der Links hier drunter, sondern aus einem Ordner hier drüber runtergeladen), dann verwende den [unten Beschriebenen CSV-Import](#karten-aktualisieren-und-hinzuf%C3%BCgen-durch-csv-import).     
**Mini-Achtung!** Sorry falls du dir ein Deck von V1.0.0 - V1.0.2 runtergeladen hast, sind leider nicht mehr kompatibel mit zukünftigen Decks wegen einem Skriptfix! (kommt hoffentlich nicht mehr vor)


# FAQ
### > Wie komme ich in den Genuss der Mathe-Maister Decks?
- **Anki runterladen** (zum Beispiel AnkiDroid im Play Store). Damit hat man die
  Software, die mit den erstellten Karten etwas anfangen kann. Das Programm ist
  anfangs komplet leer, da Anki selbstverständlich nicht weiß, was man lernen
  will!
- Gewünschte **Decks runterladen und importieren** (siehe unten). Alle mathematischen Inhalte,
  meine Designentscheidungen zum Lernerlebnis, und LaTeX-Support (braucht
  bedingt Internet, siehe unten), sind schon enthalten.
- einfach **loslernen und täglich dranbleiben**


### > Wo finde ich die Decks?
Hier:     
+ [**Analysis III** (läuft heiß)](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-III.apkg)
+ [**Geometrie I** (läuft)](https://cptmaister.gitlab.io/Mathe-Maister/Geometrie-I.apkg)
+ [**Lineare Algebra I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Eigenwerte)](https://cptmaister.gitlab.io/Mathe-Maister/Lineare-Algebra-I.apkg)
+ [**Numerik I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Interpolation durch Splines)](https://cptmaister.gitlab.io/Mathe-Maister/Numerik-I.apkg)
+ [**Seminar zu Differentialformen** (geplant sind alle wichtigen Definitionen und Sätze der Vorträge)](https://cptmaister.gitlab.io/Mathe-Maister/Seminar-Differentialformen.apkg)
+ [**Mathe Basics** (Sammlung oft vergessener Tricks und Basics)](https://cptmaister.gitlab.io/Mathe-Maister/Mathe-Basics.apkg)
+ [**Greek Alphabet** (nicht als Teil des Mathe-Maister Projekts entstanden, aber auch sehr nützlich für Mathestudenten)](https://ankiweb.net/shared/info/450594567)


### > Wo kann ich Anki runterladen?
* für den PC: https://apps.ankiweb.net/
* für Android: AnkiDroid auf
  [F-Droid](https://f-droid.org/packages/com.ichi2.anki/) oder im [Play
  Store](https://play.google.com/store/apps/details?id=com.ichi2.anki)
* für iOS: weiß nicht, aber da gibts was. ACHTUNG: Bisher wurden keine Tests auf
  iOS Anki durchgeführt!


### > Wie bleibe ich auf dem Laufenden zu großen Updates?
Dafür gibt es [*diesen E-Mail-Verteiler*](http://supersecret.me/ankiprojekt/), auf dem man sich jederzeit ein- und austragen kann.


### > Wo kann ich mir die Inhalte der Karteikarten auf einem Blick ansehen?
Hier sind die Links zu für Menschen schön lesbaren PDFs aller laufenden Stoffsammlungen. Inhaltliche Korrektheit ohne Gewähr, vollständige Sammlungen teilweise durch Promovierende o.ä. korrigiert:
+ [**Analysis III** (läuft heiß)](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-III.pdf)
+ [**Geometrie I** (läuft)](https://cptmaister.gitlab.io/Mathe-Maister/Geometrie-I.pdf)
+ [**Lineare Algebra I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Eigenwerte)](https://cptmaister.gitlab.io/Mathe-Maister/Lineare-Algebra-I.pdf)
+ [**Numerik I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Interpolation durch Splines)](https://cptmaister.gitlab.io/Mathe-Maister/Numerik-I.pdf)
+ [**Seminar zu Differentialformen** (geplant sind alle wichtigen Definitionen und Sätze der Vorträge)](https://cptmaister.gitlab.io/Mathe-Maister/Seminar-Differentialformen.pdf)
+ [**Optimierung I** (leer, geht bald los)](https://cptmaister.gitlab.io/Mathe-Maister/Optimierung-I.pdf)
+ [**Analysis I** (leer, geht zum Vorlesungsbeginn los)](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-I.pdf)
+ [**Lineare Algebra II** (leer, geht zum Vorlesungsbeginn los)](https://cptmaister.gitlab.io/Mathe-Maister/Lineare-Algebra-II.pdf)
+ [**Mathe Basics** (Sammlung oft vergessener Tricks und Basics)](https://cptmaister.gitlab.io/Mathe-Maister/Mathe-Basics.pdf)


### > Warum gibt es das?
Vor allem  nach Abschließen  einer Vorlesung vergisst  man ständig  die formale,
genaue Formulierung einiger grundlegender Definitionen  und Sätze. Da das leider
fast jedem zu oft passiert, und vor allem  mir auf den Keks geht, habe ich schon
seit  dem ersten  Semester den  Traum, welchen  ich jetzt  verwirklichen möchte:
Karteikarten gestütztes  Verfrachten aller gelernten Definitionen  und wichtiger
Sätze ins Langzeitgedächtnis, umgesetzt  durch schöne, strukturierte, effiziente
Decks in Anki.


### > Warum Karteikarten / Warum Anki?
Karteikarten (aus Papier) an sich sind schon ganz schick fürs Auswendiglernen 
von 1 zu 1 verknüpfbarem Wissen, da es pro Karte eben nur zwei Seiten gibt und 
man von der einen jeweils auf die andere Karte kommen muss. Das ist auch schon 
die einzige Limitierung, weswegen diese Decks auch nicht dafür geeignet sind, 
Konzepte neu zu erlernen, sondern schon gelernte Konzepte nicht zu vergessen, 
indem man, nach eigenem Ermessen, streng die genaue Formulierung  wiederholt, 
oder zumindest daran erinnert wird, an das vorliegende Konzept zu denken. Damit 
sind wir auch schon bei der Stärke von Anki.    
Anki funktioniert nach dem Spaced Repetition System (SRS), nach welchem einem eine
Karte in größer werdenden Abständen gezeigt wird, idealerweise genau kurz bevor
die Erinnerungen daran schwammig werden. So schaut man sich eine Karte nicht
öfter an, als nötig, aber noch bevor man sie vergisst. Je nachdem, wie gut man
sich dann an eine Karte erinnern konnte, so wird sie passend geplant für ihren
nächsten Auftritt. Anders als bei echten Karteikarten, sortiert Anki also die
Karten passend und legt einem täglich das vor, was man gerade zu vergessen droht
(+ neue Karten, die man heute zum ersten Mal sieht). Natürlich bleibt auch das
viele, schwere Papier weg und jede Karte ist beliebig voll befüllbar, in
teilweise extra anklickbaren Extra-Feldern. Und andere Funktionen, die für uns
nicht relevant sind.     
Am allerbesten ist jedoch natürlich, dass Anki, bzw
AnkiDroid (für Android, zu finden im Play Store) kostenlos ist (außer für iOS
Geräte, da kostet es bisschen viel), und dass Decks und Karten ganz einfach
verteilt und verwaltet werden können, inklusive kostenlosem Account bei Anki,
mit dem man zwischen verschiedenen Geräten synchronisieren kann.     
*Extra*: [Interaktiver Webcomic zum Spaced Repetition System](https://ncase.me/remember/)


### > Wie funktioniert Anki, was kann ich da alles machen?
Wirklich wichtige Funktionen für dieses Projekt beschreibe ich im Folgenden, den
Rest kann man sich selbst aneignen durch Rumprobieren, auf
https://apps.ankiweb.net/docs/manual.html Nachschauen, Video-Tutorials
Anschauen, Blog Posts dazu Lesen, oder mich Fragen.


### > Wie kann ich ein runtergeladenes Deck in Anki reinstecken/importieren?
- Am PC genügt es normalerweise, nachdem Anki installiert wurde, einfach auf die
  Datei vom Deck  (sollte als Endung `.apkg` haben)  doppelt zu klicken. Alternativ per
  Drag&Drop in Anki reinziehen. Oder Anki  öffnen und in der Menüleiste auf File
  -> Import... und da dann das Deck auswählen und öffnen
- Am Handy reicht es normalerweise auch, die Datei runterzuladen und dann zu
  öffnen. Bei mir zumindest wird es dann ohne Probleme von Anki erkannt und
  importiert. Ich empfehle aber, alle Decks am PC zu importieren und dann durch
  Synchronisierung aufs Handy zu bekommen. Ist aber reine Geschmackssache.


### > Wie füge ich neu veröffentlichte oder neuere Versionen von Karten zu meinem bestehenden Deck hinzu?
Einfach die neuste Version runterladen und öffnen/importieren. Es sollten alle 
Karten im alten Deck automatisch aktualisiert werden, die geändert wurden, und 
neue Karten hinzugefügt werden. Der Lernfortschritt geht dabei nicht verloren.     
Bitte eine Nachricht an mich, falls das mal nicht klappt, wir sind da noch am 
Rumprobieren!


### Karten Aktualisieren und Hinzufügen durch CSV-Import
**!!! Nur notwendig für Updates von Decks von vor Version 1 !!!**
* `CSV`-Datei zur passenden Vorlesung in der [Stoffsammlung](Stoffsammlung) runterladen
* in Anki auf das zu aktualisierende Deck klicken, dann oben links *File* → *Import...* und die Datei auf eurem Computer öffnen
* ein neues Fenster ist in Anki offen, hier auf ein paar wichtige Sachen achten:
	* Ganz oben links bei `Type` den Notiztyp *Mathe-Maister Definitionen* (könnte leicht abweichen) wählen
	* Daneben bei `Deck` das Deck, in dem ihr die Karten aktualisieren oder hinzufügen wollt
	* Gleich darunter `Fields seperated by: Tab` auswählen
	* Direkt darunter `Update existing notes when first field matches` wählen 
	* Direkt darunter Das Häkchen ☑ setzen bei `Allow HTML in fields`
	* Alles hierunter, also die Feldzuweisungen, sollten stimmen
	* ganz unten auf *Import* klicken
* in einem Ergebnisfenster seht ihr, wie viele Karten hinzugefügt und verändert wurden.     
Achtung: das erste Feld jeder Notiz muss genau übereinstimmen mit dem ersten Eintrag der Zeile, sonst entsteht eine neue Notiz, statt einem Update. Dafür nach dem Import einfach schauen, welche neue Notizen entstanden sind, entsprechende alte Notizen schnell anpassen, neu entstandene Karten wieder löschen, nochmal CSV-Import durchführen. Das ganze so oft durchführen, bis keine neue Karten mehr entstehen (nach 1 bis 2 mal hat man normalerweise alles erwischt).


### > Meine Karten werden nicht richtig angezeigt, ich sehe nur unleserliche Zeichen und Brabbeleien
Auf dem Handy einfach kurz Internet anmachen, Deck nochmal neu öffnen, dann sollte
es funktionieren bis zum kompletten Schließen der App. Danach braucht man wieder
kurz Internet, damit der Code kurz laden kann. Auf Windows PCs funktioniert 
es bisher nur manchmal, kann aber manchmal behoben werden durch Download dieses 
[Add-Ons](https://ankiweb.net/shared/info/1280253613). Auf Linuxmint funktioniert es.


### > Wie bleibe ich motiviert? / Wie kann ich die Lernerfahrung für mein leicht ablenkbares Gehirn angenehmer gestalten?
 + In den Optionen ausstellen, dass man während dem Lernen sieht, wie viele Karten noch übrig sind (lenkt sonst hart ab!)
 + [Puppy Reinforcement](https://ankiweb.net/shared/info/1722658993), ein Add-On, das einem ungefähr alle 10 Karten ein süßes Bild von einem Welpen zeigt. Enthält 50 Bilder, kann man aber nach Belieben erweitern und ersetzen. Häufigkeit auch umstellbar (Add-Ons sind generell nicht für mobile Anki-Versionen verfügbar, aber nicht vergessen: auf allen von mir bisher getesteten Linux Rechnern funktioniert das ganze Latex-Gerendere, also kann man auf einem Linux-Rechner genau so gut lernen, wie auf dem Handy, nur jetzt mit Hundewelpen!)
 + [Review Heatmap](https://ankiweb.net/shared/info/1771074083), ein Add-On, mit dem man zur gesamten eigenen Anki-Sammlung und zu einzelnen eigenen Decks die bisherigen und voraussichtlichen Wiederholungen übersichtlich betrachten kann. Sehr motivierend, da keine Lücke reinzubekommen!
 + [Progress Bar](https://ankiweb.net/shared/info/2091361802), *a progress bar that shows your progress in terms of passed cards per review session.*
 + [Life Drain](https://ankiweb.net/shared/info/715575551), *this add-on adds a life bar during your reviews. Your life reduces within time, and you must answer the questions in order to recover it.* Schön motivierend und auch so einstellbar, dass man kein Leben zurückgewinnt. Damit kann man sich ganz schön einen Timer einstellen, zum Beispiel fürs Lernen nach der [Pomodoro-Technik](https://de.wikipedia.org/wiki/Pomodoro-Technik).
 + [Night Mode für am Rechner](https://ankiweb.net/shared/info/1496166067).
 + ...mehr gibts hier nicht. Schick mir deine Lieblingstricks und -Add-Ons!


### > Hast du was zum Prokrastinieren für mich da?
Hier sind ein paar cool aussehende Decks für zwischendurch (habe ich größtenteils selbst noch nicht getestet):
[Ultimate Geography](https://ankiweb.net/shared/info/2109889812),
[Countries of the World](https://ankiweb.net/shared/info/2915332392),
[100 Colors](https://ankiweb.net/shared/info/2033234340),
[Great Works of Art](https://ankiweb.net/shared/info/685421036),
[Piano Basics](https://ankiweb.net/shared/info/347130449),
[Vogelstimmen](https://ankiweb.net/shared/info/2088996377),
[NATO phonetic alphabet](https://ankiweb.net/shared/info/766972333),
[Star Constellations](https://ankiweb.net/shared/info/1876097095),
[Fonts characteristics and history](https://ankiweb.net/shared/info/1770056221),
[Python code quiz](https://ankiweb.net/shared/info/51975584),
[5000 most common French words](https://ankiweb.net/shared/info/468622242),
[Periodic table mnemomincs](https://ankiweb.net/shared/info/490209917),
[Knots and how to tie them](https://www.memrise.com/course/649284/knots-and-how-to-tie-them/) (ist zwar auf Memrise, aber [es gibt Anki Add-Ons](https://github.com/wilddom/memrise2anki-extension), die ein Memrise Deck komplett herunterladen und zu Anki hinzufügen können).


## To Do
- [x] Projekt starten
- [ ] Co-Autoren finden *sheesh*
- [x] Skripte zur Automatisierung
    - [x] CSV zu TEX zu PDF
	- [x] Gitlab CI
    - [x] CSV zu APKG
- [x] Nichts generiertes im Repo ablegen
	- [x] PDFs
	- [x] Anki Decks
 - [ ] Vorlesungen
	- [ ] Grundlagenvorlesungen
		- [ ] LA 1: läuft
			- [x] Proto: fertig (WiSe 18/19 Uni Augsburg, bis ausschließlich Eigenwerte)
			- [ ] Korrektur
		- [ ] LA 2: ab April
		- [ ] Ana 1: ab April
		- [ ] Ana 2: ab Oktober?
		- [ ] Algebra 1: ab Oktober?
		- [ ] Geo 1: läuft
			- [ ] Proto: mitte April?
		- [ ] Opti 1: April
		- [x] Num 1: fertig (WiSe 18/19 Uni Augsburg, bis ausschließlich Interpolation durch Splines)
		- [ ] Stochastik 1: ab Oktober?
	- [ ] Weiterführende Vorlesungen
		- [ ] Ana 3: läuft
			- [ ] Proto: mitte April
		- [ ] Algebra 2: ab April 2020?
		- [ ] Topologie: ab April?
		- [ ] Opti 2: ab Oktober
		- [ ] Num 2 ???
		- [ ] Stochastik 2 ???
	- [ ] Master Vorlesungen
 - [ ] [Tims Karten](http://timbaumann.info/uni-spicker/) umsetzen
 - [ ] auf AnkiWeb SharedDecks Veröffentlichen anfangen
 - [ ] Englische Übersetzungen?


## Special Thanks
+ Felix W :owl: (Early Testing)
+ [Ingo Blechschmidt](https://gitlab.com/iblech) :turtle: (Korrektur, Motivation, ehem. Deckspeicher)
+ Julia G :bird: (Lineare Algebra I)
+ [Kilian R](https://gitlab.com/kiandru) :unicorn: (Readme-Layout-Spezialist, Blockchain Entrepreneur)
+ Leonie N :shark: (Motivation, Beratung) 
+ [Michael Struwe](https://gitlab.com/shak-mar) :frog: ([PDF Skripte](MMskripte), [CI](.gitlab-ci.yml), git Setup)
+ Moritz H :wolf: (Numerik I, [Mailing](http://supersecret.me/ankiprojekt/))
+ [Rainer E](https://gitlab.com/fanaticae) :leopard: (SQL Hacker, Python-Connaisseur, [APKG Skript](MMskripte), [CI](.gitlab-ci.yml), Ana3)
+ Roland M :dragon_face: (Korrektur Numerik I)
+ [Xaver H](https://gitlab.com/xaverdh) :dolphin:


### Kontakt
alex.m.s@gmx.de
